# https://github.com/mawww/kakoune/wiki/Normal-mode-commands
# The following keys are free for you to customise: 0, D, Y , ', #, ^, <minus>, =, <plus> and <ret>
# Place your preferred key in between the first pair of quotes where the command is found:
#   map global normal
# -------------------------------------------------------------------------------------------------- #
# Use +Esc to properly bind as the Alt-key
# iTerm2 profile rebind Alt to Esc/Option-key
# -------------------------------------------------------------------------------------------------- #
# https://discuss.kakoune.com/t/anchor-mode/84
declare-user-mode delapouite-anchor

map global delapouite-anchor 'a' '<esc><a-;>;'     -docstring 'reduce to anchor'
map global delapouite-anchor 'c' '<esc>;'          -docstring 'reduce to cursor'
map global delapouite-anchor 'f' '<esc><a-;>'      -docstring 'flip cursor and anchor'
map global delapouite-anchor 'h' '<esc><a-:><a-;>' -docstring 'ensure anchor after cursor'
map global delapouite-anchor 'l' '<esc><a-:>'      -docstring 'ensure cursor after anchor'
map global delapouite-anchor 's' '<esc><a-S>'      -docstring 'select cursor and anchor'

map global normal ';' ':<space>enter-user-mode<space>delapouite-anchor<ret>'
map global user   ';' ':<space>enter-user-mode<space>delapouite-anchor<ret>' -docstring "delapouite-anchor"

# -------------------------------------------------------------------------------------------------- #
declare-user-mode inserer

map global inserer '<c-n>' '<esc>i<c-n>' -docstring 'select next completion candidate'
map global inserer '<c-p>' '<esc>i<c-p>' -docstring 'select previous completion candidate'
map global inserer '<c-x>' '<esc>i<c-x>' -docstring 'explicit insert completion query; f, w, l'
map global inserer '<c-o>' '<esc>i<c-o>' -docstring 'disable automatic completion for this insert session'
map global inserer '<c-r>' '<esc>i<c-r>' -docstring 'insert contents of the register given by next key'
map global inserer '<c-v>' '<esc>i<c-v>' -docstring 'insert next keystroke directly into the buffer, without interpreting it'
map global inserer '<c-u>' '<esc>i<c-u>' -docstring 'commit changes up to now as a single undo group'
map global inserer '<a-;>' '<esc>i<a-;>' -docstring 'escape to normal mode for a single command'

# map global inserer '' '' -docstring ''

map global normal 'Y' ':<space>enter-user-mode<space>inserer<ret>'
map global user   'Y' ':<space>enter-user-mode<space>inserer<ret>' -docstring "inserer       | keyboard shortcuts"

# -------------------------------------------------------------------------------------------------- #
declare-user-mode une-changes

map global une-changes 'c' '<esc>c'    -docstring 'yank and delete current selection and enter insert mode'
map global une-changes 'd' '<esc>d'    -docstring 'yank and delete current selection'
map global une-changes 'y' '<esc>y'    -docstring 'yank selections'
map global une-changes 'a' '<esc>a'    -docstring 'enter insert mode after current selection'
map global une-changes 'A' '<esc>A'    -docstring 'enter insert mode at current selection end line end'
map global une-changes 'i' '<esc>i'    -docstring 'enter insert mode before current selection'
map global une-changes 'I' '<esc>I'    -docstring 'enter insert mode at current selection begin line start'
map global une-changes 'o' '<esc>o'    -docstring 'enter insert mode in one (or given count) new lines below current selection end'
map global une-changes 'O' '<esc>O'    -docstring 'enter insert mode in one (or given count) new lines above current selection begin'
map global une-changes 'p' '<esc>p'    -docstring 'paste after current selection end'
map global une-changes 'P' '<esc>P'    -docstring 'paste before current selection begin'
map global une-changes 'r' '<esc>r'    -docstring 'replace each character with the next entered one'
map global une-changes 'R' '<esc>R'    -docstring 'replace current selection with yanked text'
map global une-changes 'u' '<esc>u'    -docstring 'undo last change'
map global une-changes 'U' '<esc>U'    -docstring 'redo last change'
map global une-changes '`' '<esc>`'    -docstring 'to lower case'
map global une-changes '~' '<esc>~'    -docstring 'to upper case'
map global une-changes '!' '<esc>!'    -docstring 'insert command output before selection'
map global une-changes '@' '<esc>@'    -docstring 'convert tabs to spaces in current selections, buffer tabstop or count parameter'
map global une-changes '&' '<esc>&'    -docstring 'align selection, align the cursor of selections by inserting spaces'
map global une-changes '|' '<esc>|'    -docstring 'pipe selection through an external filter program and replace with its output'
map global une-changes '<' '<esc><lt>' -docstring 'dedent selected lines'
map global une-changes '>' '<esc><gt>' -docstring 'indent selected lines'
map global une-changes '.' '<esc>.'    -docstring 'repeat last insert mode change (i, a, or c, including the inserted text)'

# map global une-changes . '' -docstring ''

map global normal '=' ':<space>enter-user-mode<space>une-changes<ret>'
map global user   '=' ':<space>enter-user-mode<space>une-changes<ret>' -docstring "une-changes   | keyboard shortcuts"

# -------------------------------------------------------------------------------------------------- #
declare-user-mode deux-changes

map global deux-changes '<a-c>'  '<esc><a-c>'   -docstring 'delete current selection and enter insert mode'
map global deux-changes '<a-d>'  '<esc><a-d>'   -docstring 'delete current selection'
map global deux-changes '<a-m>'  '<esc><a-m>'   -docstring 'merge contiguous selections together (works across lines as well)'
map global deux-changes '<a-R>'  '<esc><a-R>'   -docstring 'replace current selection with every yanked text'
map global deux-changes '<a-j>'  '<esc><a-j>'   -docstring 'join selected lines'
map global deux-changes '<a-J>'  '<esc><a-J>'   -docstring 'join selected lines and select spaces inserted in place of line breaks'
map global deux-changes '<a-o>'  '<esc><a-o>'   -docstring 'add an empty line below cursor'
map global deux-changes '<a-O>'  '<esc><a-O>'   -docstring 'add an empty line above cursor'
map global deux-changes '<a-p>'  '<esc><a-p>'   -docstring 'paste all after the end of each selection, and select each pasted string'
map global deux-changes '<a-P>'  '<esc><a-P>'   -docstring 'paste all before the start of each selection, and select each pasted string'
map global deux-changes '<a-u>'  '<esc><a-u>'   -docstring 'move backward in history'
map global deux-changes '<a-U>'  '<esc><a-U>'   -docstring 'move forward in history'
map global deux-changes '<a-`>'  '<esc><a-`>'   -docstring 'swap case'
map global deux-changes '<a-!>'  '<esc><a-!>'   -docstring 'append and select command output after each selection'
map global deux-changes '<a-@>'  '<esc><a-@>'   -docstring 'convert spaces to tabs in current selections'
map global deux-changes '<a-&>'  '<esc><a-&>'   -docstring 'copy indent, copy the indentation of the main selection to all other ones'
map global deux-changes '<a-(>'  '<esc><a-(>'   -docstring 'rotate selections content backwards'
map global deux-changes '<a-)>'  '<esc><a-)>'   -docstring 'rotate selections content, if specified, the count groups selections'
map global deux-changes '<a-|>'  '<esc><a-|>'   -docstring 'pipe selection through the given external filter program and ignore its output'
map global deux-changes '<a-lt>' '<esc><a-lt>'  -docstring 'deindent selected lines, do not remove incomplete indent'
map global deux-changes '<a-gt>' '<esc><a-gt>'  -docstring 'indent selected lines, including empty lines'

# map global deux-changes '<>' '<>' -docstring ''

map global normal "<plus>" ':<space>enter-user-mode<space>deux-changes<ret>'
map global user   "<plus>" ':<space>enter-user-mode<space>deux-changes<ret>' -docstring "deux-changes  | keyboard shortcuts"

# -------------------------------------------------------------------------------------------------- #
declare-user-mode une-movement

map global une-movement 'l' '<esc>l' -docstring 'select the character on the right of selection end'
map global une-movement 'h' '<esc>h' -docstring 'select the character on the left of selection end'
map global une-movement 'k' '<esc>k' -docstring 'select the character above the selection end'
map global une-movement 'j' '<esc>j' -docstring 'select the character below the selection end'
map global une-movement 'w' '<esc>w' -docstring 'select the word and following whitespaces on the right of selection end'
map global une-movement 'e' '<esc>e' -docstring 'select preceding whitespaces and the word on the right of selection end'
map global une-movement 'b' '<esc>b' -docstring 'select preceding whitespaces and the word on the left of selection end'
map global une-movement 'f' '<esc>f' -docstring 'select to (including) the next occurrence of the given character'
map global une-movement 't' '<esc>t' -docstring 'select until (excluding) the next occurrence of the given character'
map global une-movement 'm' '<esc>m' -docstring 'select matching character'
map global une-movement 'M' '<esc>M' -docstring 'extend selection to matching character'
map global une-movement 'x' '<esc>x' -docstring 'expand selections to contain full lines (including end-of-lines)'
map global une-movement '%' '<esc>%' -docstring 'select whole buffer'
map global une-movement '/' '<esc>/' -docstring 'search (select next match)'
map global une-movement '?' '<esc>?' -docstring 'search (extend to next match)'
map global une-movement 'n' '<esc>n' -docstring 'select next match'
map global une-movement 'N' '<esc>N' -docstring 'add a new selection with previous match'
map global une-movement ')' '<esc>)' -docstring 'rotate selections (main selection becomes the next one)'
map global une-movement '(' '<esc>(' -docstring 'rotate selections backwards'
map global une-movement '_' '<esc>_' -docstring 'trim selections'
map global une-movement ';' '<esc>;' -docstring 'reduce selections to their cursor'

# map global une-movement . '' -docstring ''

map global normal '<minus>' ':<space>enter-user-mode<space>une-movement<ret>'
map global user   '<minus>' ':<space>enter-user-mode<space>une-movement<ret>' -docstring "une-movement  | keyboard shortcuts"

# -------------------------------------------------------------------------------------------------- #
declare-user-mode deux-movement

map global deux-movement '<a-w>' '<esc><a-w>' -docstring 'same as [w] but select WORD instead of word'
map global deux-movement '<a-b>' '<esc><a-b>' -docstring 'same as [b] but select WORD instead of word'
map global deux-movement '<a-e>' '<esc><a-e>' -docstring 'same as [e] but select WORD instead of word'
map global deux-movement '<a-f>' '<esc><a-f>' -docstring 'same as [f] but in the other direction'
map global deux-movement '<a-t>' '<esc><a-t>' -docstring 'same as [t] but in the other direction'
map global deux-movement '<a-x>' '<esc><a-x>' -docstring 'trim selections to only contain full lines (not including last end-of-line)'
map global deux-movement '<a-h>' '<esc><a-h>' -docstring 'select to line begin'
map global deux-movement '<a-l>' '<esc><a-l>' -docstring 'select to line end'
map global deux-movement '<a-/>' '<esc><a-/>' -docstring 'search (select previous match)'
map global deux-movement '<a-?>' '<esc><a-?>' -docstring 'search (extend to previous match)'
map global deux-movement '<a-n>' '<esc><a-n>' -docstring 'select previous match'
map global deux-movement '<a-N>' '<esc><a-N>' -docstring 'add a new selection with previous match'
map global deux-movement '<a-:>' '<esc><a-:>' -docstring 'ensure selections are in forward direction (cursor after anchor)'
map global deux-movement '<a-.>' '<esc><a-.>' -docstring 'repeat last object or f/t selection command'
map global deux-movement '<a-;>' '<esc><a-;>' -docstring 'flip the selections direction'
map global deux-movement '<c-b>' '<esc><c-b>' -docstring 'scroll one page up'
map global deux-movement '<c-f>' '<esc><c-f>' -docstring 'scroll one page down'
map global deux-movement '<c-u>' '<esc><c-u>' -docstring 'scroll half page up'
map global deux-movement '<c-d>' '<esc><c-d>' -docstring 'scroll half page down'

# map global deux-movement . '<>' -docstring ''

map global normal "'" ':<space>enter-user-mode<space>deux-movement<ret>'
map global user   "'" ':<space>enter-user-mode<space>deux-movement<ret>' -docstring "deux-movement | keyboard shortcuts"

