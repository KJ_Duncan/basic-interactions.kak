## Basic Keyboard Interactions ##


A Kakoune plugin that maps basic keyboard shortcuts to the editors [normal user mode](https://github.com/mawww/kakoune#normal-mode) in order to help the new user, part-timer, and others.  

An in editor information box displays a quick selection list dependent on the users initial keystroke `;` or `Y` or `=` or `+` or `-` or `'` while in the editors normal user mode.  
The quick selection list displays the Kakoune key sequence and related usage description.  

For the full list of commands this plugin makes available please read the source code in rc/basic-interactions.kak, and feel free to clone it, change it, and rearrange it.  
The source code file provides alternate initial key implementation for your choosing.  

Too many keys to remember all at once?  
Just use the `<space>` bar in the editors normal user mode to select one of the available shortcut modes.  


```
Lets get dangerous kakoune commit >=
commit 674053935d3260dd54c4e0084c76e9170d731eb2
Date:   Sat Sep 10 16:05:37 2022 +1000

See the section below 'Kakoune Build'
and its reference to 'Kakoune changelog'
for key changes > Kakoune 2021.11.07
```  
  
  
OFFAIAH 2018, _'Push Pull'_, Defected Records, Spotify, view 11 September 2022, <https://open.spotify.com/track/3wGCJz3Ay0tLt3JdkjVzmB?si=66658ecc082e4e1d>  


### Plugin Examples ###


  
* declare-user-mode `;` [**delapouite-anchor**](https://discuss.kakoune.com/t/anchor-mode/84)

            map global delapouite-anchor a '<esc><a-;>;' -docstring 'reduce to anchor'  
            map global delapouite-anchor c '<esc>;'      -docstring 'reduce to cursor'
  
![](/res/delapouite-anchor.png)  
  
* declare-user-mode `Y` [**inserer**](https://github.com/mawww/kakoune#33-insert-mode)

            map global inserer '<c-n>' '<esc>i<c-n>' -docstring 'select next completion candidate'  
            map global inserer '<c-p>' '<esc>i<c-p>' -docstring 'select previous completion candidate'
  
![](/res/inserer.png)    
  
* declare-user-mode `=` [**une-changes**](https://github.com/mawww/kakoune#changes)

            map global une-changes r 'r'    -docstring 'replace each character with the next entered one'  
            map global une-changes R 'R'    -docstring 'replace current selection with yanked text'
  
![](/res/une-changes.png)  
  
* declare-user-mode `+` [**deux-changes**](https://github.com/mawww/kakoune#changes)

            map global deux-changes <a-j> ’<a-j>’  -docstring ’join selected lines’  
            map global deux-changes <a-J> '<a-J>'  -docstring 'join selected lines and select spaces inserted in place of line breaks'
  
![](/res/deux-changes.png)  
  
* declare-user-mode `-` [**une-movement**](https://github.com/mawww/kakoune#movement)

            map global une-movement f 'f'   -docstring 'select to (including) the next occurrence of the given character'  
            map global une-movement t 't'   -docstring 'select until (excluding) the next occurrence of the given character'
  
![](/res/une-movement.png)  
  
* declare-user-mode `'` [**deux-movement**](https://github.com/mawww/kakoune#movement)

            map global deux-movement <a-n> ’<a-n>’ -docstring ’select previous match’  
            map global deux-movement <a-N> '<a-N>' -docstring 'add a new selection with previous match'  
  
![](/res/deux-movement.png)  
  
  
---


#### Install #### 


[Kakoune wiki Plugins](https://github.com/mawww/kakoune/wiki#plugins)  
  
A good guy and well supported plugin manager [plug.kak](https://github.com/andreyorst/plug.kak)  
```
plug "KJ_Duncan/basic-interactions.kak" domain "bitbucket.org"
```


#### Alternate Setup ####


In [rc/basic-interactions.kak](https://bitbucket.org/KJ_Duncan/basic-interactions.kak/src/master/rc/basic-interactions.kak) remove column five prefix `<esc>` to deactivate commands from the info-box.  
  
Before:  
  
```
map global une-changes 'c' '<esc>c' -docstring 'yank and delete current selection and enter insert mode'  
------------------------------^  
  
map global deux-changes '<a-c>' '<esc><a-c>' -docstring 'delete current selection and enter insert mode'  
-----------------------------------^  
  
map global une-movement 'l' '<esc>l' -docstring 'select the character on the right of selection end'  
-------------------------------^  
  
map global deux-movement '<a-w>' '<esc><a-w>' -docstring 'same as [w] but select WORD instead of word'  
------------------------------------^  
  
```  
  
After:  
  
```
map global une-changes 'c' 'c' -docstring 'yank and delete current selection and enter insert mode'  
----------------------------^  
  
map global deux-changes '<a-c>' '<a-c>' -docstring 'delete current selection and enter insert mode'  
-----------------------------------^  
  
map global une-movement 'l' 'l' -docstring 'select the character on the right of selection end'  
-----------------------------^  
  
map global deux-movement '<a-w>' '<a-w>' -docstring 'same as [w] but select WORD instead of word'  
------------------------------------^  
```  


##### Kakoune Build #####


Kakoune has a stable code base in it's master branch to build from. No need to wait for a release.  
The process is a complex problem. So, let's break up into it's simple parts.  


1. git clone kakoune
2. kakoune build tools
3. kakoune configuration
4. git clone basic-interactions.kak
5. running kakoune


References:  

* git-clone Clone a repository into a new directory https://git-scm.com/docs/git-clone
* Kakoune 2.1 Building https://github.com/mawww/kakoune#21-building
* GCC and Make Compiling, Linking and Building C/C++ Applications https://www3.ntu.edu.sg/home/ehchua/programming/cpp/gcc_make.html
* GNU Make https://www.gnu.org/software/make/
* Kakoune 2.3.1. Configuration https://github.com/mawww/kakoune#231-configuration
* Kakoune Running commands at startup https://github.com/mawww/kakoune/wiki/Running-commands-at-startup
* "Pretty print" $PATH, separate path per line https://www.commandlinefu.com/commands/view/13697/pretty-print-path-separate-path-per-line
* Kakoune 2.3. Running https://github.com/mawww/kakoune#23-running
* Kakoune changelog https://github.com/mawww/kakoune/blob/master/doc/pages/changelog.asciidoc

  
```shell
# lets start with 
$ git clone https://github.com/mawww/kakoune.git
$ cd kakoune/src

# this build environment for kakoune works for me
$ export CXX="g++-12"
$ make
$ make man

# create a new symbolic link 'kak' that points to 'kak.opt'
$ file kak.opt
> kak.opt: ... executable, ...

# to any folder on your path 
$ echo $PATH | tr -s ':' '\n'

# for most people it's convenient in either $HOME/local/bin OR $HOME/.local/bin
$ ln -s $PWD/kak.opt $HOME/local/bin/kak

# now change back to a directory out of kakoune/src
$ cd $HOME

###
# next you will first need to read and implement the above Kakoune 2.3.1. Configuration
# further reading can be found at the Kakoune Running commands at startup reference
###

# now let's do
$ git clone https://bitbucket.org/KJ_Duncan/basic-interactions.kak.git
$ cd basic-interactions.kak
$ cp rc/basic-interactions.kak $HOME/.config/kak/autoload/
# OR
$ cp rc/basic-interactions.kak $XDG_CONFIG_HOME/kak/autoload/

# 2.3. Running mawww's experiment for a better code editor
$ kak rc/basic-interactions.kak
```


##### Kakoune General Information #####


Work done? Have some fun. Share any improvements, ideas or thoughts with the community either directly on [discuss.kakoune](https://discuss.kakoune.com/).  
  
Kakoune is an open source modal editor. The source code lives on github [mawww/kakoune](https://github.com/mawww/kakoune#-kakoune--).  
A discussion on the repository structure of _’community plugins’_ for Kakoune can be reviewed here: [Standardi\(s|z\)ation of plugin file-structure layout #2402](https://github.com/mawww/kakoune/issues/2402).  
Read the Kakoune wiki for information on install options via a [Plugin Manager](https://github.com/mawww/kakoune/wiki/Plugin-Managers).  
  
Thank you to the Kakoune community 300+ contributors for a great modal editor in the terminal. I use it daily for the keyboard shortcuts.  
  
  
---
  
That's it for the readme, anything else you may need to know just pick up a book and read it [Polar Bookshelf](https://getpolarized.io/). Thanks all. Bye.
